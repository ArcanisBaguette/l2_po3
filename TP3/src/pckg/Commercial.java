package pckg;

public class Commercial extends Employe {
	private double fixe = -1.0, ca = -1.0;
	private static final double pourcentageCA=1.0;
	
	public Commercial(String nom) 
	{
		this.nom=nom;
	}
	
	public Commercial(String nom, double fixe, double ca) 
	{
		this.nom=nom;
		setInfoSalaire(fixe,ca);
	}
	
	public void setInfoSalaire(double fixe, double ca) 
	{
		this.fixe=fixe;
		this.ca = ca;
	}
	
	public double getSalaire() 
	{
		return fixe + pourcentageCA * ca;
	}
}
