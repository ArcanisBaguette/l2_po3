package pckg;

public class EmployeHoraire extends Employe{
	private double heure;
	private double tarif_h;
	private double pourc_h_supp;
	public EmployeHoraire(String nom) {this.nom=nom;}
	
	public EmployeHoraire(String nom, double heure, double tarif_h, double pourc_h_supp) 
	{
		this.nom=nom;
		setInfoSalaire(heure,tarif_h,pourc_h_supp);
	}
	
	public void setInfoSalaire(double heure, double tarif_h, double pourc_h_supp) 
	{
		this.heure = heure;
		this.tarif_h = tarif_h;
		this.pourc_h_supp = pourc_h_supp;
	}
	
	public double getSalaire() 
	{
		if(heure<35.0)
			System.out.println(heure*tarif_h);
		else
			System.out.println(tarif_h*35 + pourc_h_supp * tarif_h * (heure-35));
		return heure<35.0 ? heure*tarif_h : tarif_h*35 + pourc_h_supp * tarif_h * (heure-35);
	}
}
