package pckg;

public class Paie {
	
	public static void main(String[] args) {
		Employe[] tab = new Employe[4];
		
		tab[0]= new EmployeHoraire("Corentin", 10.0, 40.0, 0.5);
		tab[1]= new EmployeHoraire("Lillian", 10.0, 30.0, 0.3);
		tab[2]= new Commercial("Laurent", 10000.0, 150000.0);
		tab[3]= new Commercial("Bernard", 2000.0, 30000000.0);
		
		for(Employe e : tab) 
		{
			System.out.println(e.getSalaire());
		}
	}
	
}
