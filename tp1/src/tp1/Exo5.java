package tp1;

public class Exo5 {
	public static int imageMiroir(int n) 
	{
		return Integer.parseInt(new StringBuilder(String.valueOf(n)).reverse().toString());
	}
	
	public static int imageMiroir2(int n) 
	{
		int miroir = n%10;
		n/=10;
		while(n!=0)
		{
			miroir *= 10;
			miroir += n%10;
			n/=10;
		}
		return miroir;
	}
	public static void main(String[] args) {
		System.out.println(imageMiroir(1234));
		
		System.out.println(imageMiroir2(1234));
	}
}
