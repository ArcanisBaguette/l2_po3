package tp1;

public class Exo2 {
	public static int compteMal(int n) 
	{
		int compteur = 0;
		for(int i=0; i<=n; i++)
			compteur+=i;
		return compteur;
	}
	
	public static void main(String[] args) {
		int n= 5;
		int resultat = compteMal(n);
		System.out.println(resultat);
	}
}
