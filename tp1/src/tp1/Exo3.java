package tp1;

public class Exo3 {
	public static boolean estPresentEn(int k, String s1, String s) 
	{
		//en utlisant des méthodes déjà existantes :
		try 
		{
			return s1.contentEquals(s.substring(k,k+s1.length()));
		}
		catch(StringIndexOutOfBoundsException e)
		{
			return false;
		}
	}
	
	public static int indiceDe(String s1, String s) 
	{
		for(int i = 0;i<s1.length();i++)
			if(estPresentEn(i,s1,s))
				return i;
		return -1;
	}
	
	public static void main(String[] args) {
		System.out.println(estPresentEn(0,"bon","bonjour"));
		System.out.println(estPresentEn(8,"bon","bonjour bonsoir"));
		System.out.println(estPresentEn(20,"bon","bonjour bonsoir"));
		
		System.out.println(indiceDe("soir","bonsoir"));
		
	}
}
