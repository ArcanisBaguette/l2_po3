package pckg;

public class Enclos {
	private static final int TAILLE_ENCLOS = 5;
	private int capacite_actuel;
	private Animal[] animaux ;
	private Class type_animaux;
	
	Enclos()
	{
		animaux = new Animal[TAILLE_ENCLOS];
	}
	
	public boolean ajouterAnimal(Animal a) 
	{
		if(capacite_actuel==TAILLE_ENCLOS)
			return false;
		
		//prend la premiere case vide
		
		if(capacite_actuel==0)
			type_animaux = a.getClass().getSuperclass();
		else if(type_animaux != a.getClass().getSuperclass())
			return false;			
		
		
		for(int i=0; i!=TAILLE_ENCLOS; i++) 
			if(animaux[i]==null)
				animaux[capacite_actuel]=a;
		capacite_actuel++;
		
		return true;
	}
	
	public boolean supprimerAnimal(Animal a) 
	{
		boolean test = false;
		
		for(int i=0; i!=capacite_actuel; i++) 
		{
			if(a.equals(animaux[i])) 
			{
				test=true;
				animaux[i]=null;
			}
		}
		
		return test;
	}
	
	public Animal[] getAnimaux() 
	{
		return animaux;
	}
	
	
}
