package pckg;

public abstract class Cervide extends Animal{
	Cervide(int age_, String nom_, Animal parent_)
	{
		super(age_, nom_,parent_);
	}

	@Override
	public int getLongevite() {
		return 20;
	}
}
