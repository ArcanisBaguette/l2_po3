package pckg;

public abstract class Oiseau extends Animal{
	private int nombre_spectacle;
	
	public int getNombre_spectacle() {
		return nombre_spectacle;
	}

	public void setNombre_spectacle(int nombre_spectacle) {
		this.nombre_spectacle = nombre_spectacle;
	}

	Oiseau(int age_, String nom_, Animal parent_)
	{
		super(age_, nom_, parent_);
		nombre_spectacle=0;
	}
	
	public String toString() 
	{
		return super.toString() + " A particip� " + nombre_spectacle + " fois � un spectacle. ";
	}
}
