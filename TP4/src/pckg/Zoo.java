package pckg;

import java.util.HashMap;

public class Zoo {
	private int nombre_enclos;
	private int capacite_actuel;
	private String nom;
	private Enclos[] enclos;
	
	Zoo(String nom_, int nombre_enclos_)
	{
		nombre_enclos = nombre_enclos_;
		
		enclos = new Enclos[nombre_enclos_];
		for(int i=0; i!= nombre_enclos;i++)
			enclos[i] = new Enclos();
			
			
			
		nom=nom_;
		capacite_actuel=0;
	}
	
	public boolean ajouter_animal(Animal a) 
	{
		//on essaye de trouver un enclos qui peut accepter l'animal
		for(Enclos e : enclos) 
		{
			if(e.ajouterAnimal(a)) //si on a reussi a ajouter l'animal (i.e il y avait assez de place et le bon type d'animaux)
			{
				capacite_actuel++;
				return true;				
			}
				
		}
		
		return false;
	}
	
	public boolean supprimer_animal(Animal a) 
	{
		for(Enclos e : enclos) 
		{
			if(e.supprimerAnimal(a)) 
			{
				capacite_actuel--;
				return true;				
			}
				
		}
		
		return false;
	}
	
	public Animal[] getAnimaux() 
	{
		Animal[] tab = new Animal[nombre_enclos*5];
		int compteur =0;
		
		//on parcourt par enclos puis par animal
		
		for(int i=0; i!=nombre_enclos; i++) 
		{
			for(Animal a : enclos[i].getAnimaux()) 
			{
				if(a!=null) 
				{
					tab[compteur]=a;
					compteur++;
				}
			}
		}
		
		return tab;
	}
	
	/**
	 * 
	 * @return l'animal le plus ag�
	 */
	public Animal doyen() 
	{
		if(capacite_actuel==0)
			return null;
		
		Animal doyen = null; //pass� ce point l'animal renvoy� ne peut pas �tre null car il y a forc�ment au moins 1 animal dans le zoo
		int age_doyen=-1;
		
		Animal[] tab = getAnimaux();
		
		for(Animal temp_a : tab) 
		{
			if(temp_a!=null)
				if(temp_a.getAge()>age_doyen) 
				{
					age_doyen=temp_a.getAge();
					doyen = temp_a;
				}
		}
		
		return doyen;
	}
	
	public double age_moyen() 
	{
		if(capacite_actuel==0)
			return 0;
		
		double somme=0;
		
		Animal[] tab = getAnimaux();
		
		for(Animal temp_a : tab) 
		{
			if(temp_a!=null)
				somme+=temp_a.getAge();
		}
		
		return somme/capacite_actuel;
	}
	
	public Animal[] enfants() 
	{
		Animal[] tab= new Animal[capacite_actuel]; 
		int compteur = 0;
		//toutes les cellules ne seront peut �tre pas remplies (on ne sait pas combien il y a d'enfants)
		
		Animal[] animaux = getAnimaux();
		
		for(Animal temp_a : animaux) 
		{
			if(temp_a!=null)
				if(temp_a.getAge()<0.25*temp_a.getLongevite()) 
				{
					tab[compteur]=temp_a;
					compteur++;
				}	
		}
		return tab;
	}
	
	public Animal trouverOiseauLeMoinsFatigue() 
	{
		Animal oiseau_en_forme = null;
		int nb_spectacle = Integer.MAX_VALUE;
		
		Animal[] animaux = getAnimaux();
		
		for(Animal temp_a : animaux) 
		{
			if(temp_a instanceof Oiseau) 
			{
				int temp_nb_spect = ((Oiseau) temp_a).getNombre_spectacle();
				if(temp_nb_spect<nb_spectacle) 
				{
					nb_spectacle=temp_nb_spect;
					oiseau_en_forme=temp_a;
				}
			}
				
		}
		
		return oiseau_en_forme;
	}
	
	public void creerSpectacle(Oiseau o) 
	{
		o.setNombre_spectacle(o.getNombre_spectacle()+1);
		
		System.out.println(o.getNom() + " participe a un spectacle.");
	}
	
	public Class especeLaPlusRepresente() 
	{
		HashMap<Class, Integer> map = new HashMap<>();
		
		Animal[] animaux = getAnimaux();
		
		for(Animal temp_a : animaux) 
		{
			if(temp_a!=null)
			{
				Class c = temp_a.getClass();
				if(map.containsKey(c)) 
				{
					map.replace(c, map.get(c)+1);
				}
				else 
				{
					map.put(c, 1);
				}
			}
		}
		
		
		
		HashMap.Entry<Class,Integer> max = null;
		for(HashMap.Entry<Class,Integer> entree : map.entrySet()) 
		{
			if(max==null || entree.getValue() > max.getValue())
				max = entree;
				
		}
		//notre map remplie, il suffit maintenant de cherche le max
		
		return max.getKey();
	}
	
	public Class familleLaPlusRepresente()
	{
		HashMap<Class, Integer> map = new HashMap<>();
		
		Animal[] animaux = getAnimaux();
		
		for(Animal temp_a : animaux) 
		{
			if(temp_a!=null)
			{
				Class c = temp_a.getClass().getSuperclass();
				if(map.containsKey(c)) 
				{
					map.replace(c, map.get(c)+1);
				}
				else 
				{
					map.put(c, 1);
				}
			}
		}
		
		
		
		HashMap.Entry<Class,Integer> max = null;
		for(HashMap.Entry<Class,Integer> entree : map.entrySet()) 
		{
			if(max==null || entree.getValue() > max.getValue())
				max = entree;
				
		}
		//notre map remplie, il suffit maintenant de cherche le max
		
		return max.getKey();
	}
	
	
}
