package pckg;

public class Main {
	public static void main(String[] args) {
		Animal a1 = new Lion(-1, "Simon", null);
		System.out.println(a1);
		
		Felin f1 = new Lion(100 ,"Cedric", null);
		System.out.println(f1);
		
		Animal a2 = new Perroquet(18, "Albert", null);
		Animal a3 = new Faucon(4, "Albert2", null);
		
		Animal f11 = new Lion(2, "CedricJr", f1);
		
		Zoo z = new Zoo("Zoo de Rennes 1", 4);
		
		z.ajouter_animal(a1);
		z.ajouter_animal(a2);
		z.ajouter_animal(a3);
		z.ajouter_animal(f1);
		
		System.out.println(z.especeLaPlusRepresente().getSimpleName() + " est l'espece la plus représentée.");
		System.out.println(z.familleLaPlusRepresente().getSimpleName() + " est la famille la plus représentée.");
		
		for(int i=0; i!=5; i++)
			z.creerSpectacle((Oiseau)z.trouverOiseauLeMoinsFatigue());
		
		z.ajouter_animal(f11);
		
		System.out.println(a2);
		
		System.out.println(z.doyen() + " C'est le doyen du zoo.");
		
		System.out.println("Je suis : " + f11 + " Mon parent est : " + f11.getParent());
		
		System.out.println("Voici la liste des animaux enfants :");
		
		for(Animal a : z.enfants())
			if(a!=null)
				System.out.println(a);
		
		z.supprimer_animal(f11);
		
		System.out.println("Voici la liste des animaux enfants :");
		
		for(Animal a : z.enfants())
			if(a!=null)
				System.out.println(a);
	}
}
