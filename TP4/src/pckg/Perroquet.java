package pckg;

public class Perroquet extends Oiseau{

	public Perroquet(int age_, String nom_, Animal parent_) {
		super(age_, nom_, parent_);
	}
	
	public String toString() 
	{
		return super.toString() + " C'est un " + this.getClass().getSuperclass().getSimpleName() + ". C'est un " + this.getClass().getSimpleName();
	}
	
	@Override
	public int getLongevite() {
		return 45;
	}

}
