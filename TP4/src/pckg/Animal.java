package pckg;

public abstract class Animal {
	private String nom;
	private int age;
	private Animal parent;
	
	Animal(int age_, String nom_, Animal parent_)
	{
		age=age_;
		nom=nom_;
		parent=parent_;
	}

	public Animal getParent() {
		return parent;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString() 
	{
		return nom + " a " + age + " ans.";
	}
	
	public abstract int getLongevite();
	
}
