package pckg;

public class Cerf extends Cervide{
	Cerf(int age_, String nom_, Animal parent_)
	{
		super(age_, nom_, parent_);
	}
	
	public String toString() 
	{
		return super.toString() + " C'est un " + this.getClass().getSuperclass().getSimpleName();
	}
	
	
}
