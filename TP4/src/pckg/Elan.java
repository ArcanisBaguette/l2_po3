package pckg;

public class Elan extends Cervide{
	Elan(int age_, String nom_, Animal parent_)
	{
		super(age_, nom_, parent_);
	}
	
	public String toString() 
	{
		return super.toString() + " C'est un " + this.getClass().getSuperclass().getSimpleName() + ". C'est un " + this.getClass().getSimpleName();
	}
}
