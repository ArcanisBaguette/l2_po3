package pckg;

public class Sudoku {
	/*
	 * 
	 * L2 - PO, TP n 2 
	 * Auteur : Bouchard Corentin
	 * 
	 */
	static final int n = 3 ;		// taille des regions
	/*
	 * Terminologie
	 * 
	 * m est un plateau (de sudoku) si
	 * 	- m est un int [][] ne contenant que des entiers compris entre 0 et 9
	 * 	- m.length = n^2
	 *  - m[i].length = n^2 pour tous les i de 0 a n^2-1
	 *  
	 */

	static String enClair (int [][] m) {
		/*
		 * Prerequis : m est un plateau de sudoku
		 * Resultat : une chaine dont l'affichage permet de visualiser m
		 * 
		 */
		String r = "" ;		
		for (int i = 0; i < n*n ; i++) {
			for (int j = 0; j < n*n ; j++) {
				r = r + m[i][j] + " " ;
				if (j%n == n-1) {r = r + "  ";}
			}
			if (i%n == n-1) {r = r + "\n";}
			r = r + "\n";
		}		
		r = r + " " ;		
		return r ;
	} // enClair
	
	static int [][] aPartirDe (String s) {
		/*
		 * Prerequis : s est une chaine contenant au moins n^4 chiffres decimaux
		 * Resultat : un plateau de sudoku initialise avec les n^4 premiers chiffres
		 * decimaux de s (les chiffres sont consideres comme ranges par lignes).
		 */
		int [][] m = new int [n*n][n*n] ;
		int k = 0 ;
		for (int i = 0; i < m.length ; i++) {
			for (int j = 0; j < m[i].length ; j++) {
				while ("0123456789".indexOf(s.charAt(k))==-1) {k++;}
				m[i][j] = (int) s.charAt(k) - (int) '0' ;
				k++ ;
			}			
		}
		return m ;
		
	} // aPartirDe
	
	static boolean presentLigne (int [][] m, int v, int i) {
		/*
		 * Prerequis :
		 *  - m est un plateau de sudoku
		 *  - v est compris entre 1 et n^2
		 *  - i est compris entre 0 et n^2-1
		 * Resultat : dans m, v est present dans la ligne i
		 * 
		 */
		for(int colonne =0; colonne != n*n; colonne++) 
		{
			if(m[i][colonne]==v)
				return true;
		}
		
		return false ;
	} // presentLigne
	
	static boolean presentColonne (int [][] m, int v, int j) {
		/*
		 * Prerequis :
		 *  - m est un plateau de sudoku
		 *  - v est compris entre 1 et n^2
		 *  - j est compris entre 0 et n^2-1
		 * Resultat : dans m, v est present dans la colonne j
		 * 
		 */
		for(int ligne=0; ligne != n*n; ligne++) 
		{
			if(m[ligne][j]==v)
				return true;
		}
		return false ; 
	} // presentColonne
	
	static boolean presentRegion  (int [][] m, int v, int i, int j) {
		/*
		 * Prerequis :
		 *  - m est un plateau de sudoku
		 *  - v est compris entre 1 et n^2
		 *  - i et j sont compris entre 0 et n^2-1
		 * Resultat : dans m, v est present dans la region contenant la case <i, j>
		 * 
		 */
		
		//on commence par chercher le coin gauche de la r�gion
		
		int coin_ligne   = (i/n)*n;
		int coin_colonne = (j/n)*n;
		
		//on regarde toutes les cases de la r�gion
		
		for(int a=0; a!=n; a++) 
		{
			for(int b=0; b!=n; b++) 
			{
				if(m[coin_ligne+a][coin_colonne+b]==v) 
				{
					return true;
				}
					
			}
		}
		
		return false ; 
	} // presentRegion
	
	static boolean [] lesPossiblesEn (int [][] m, int i, int j) {
		/*
		 * Prerequis :
		 *  - m est un plateau de sudoku
		 *  - i et j sont compris entre 0 et n^2-1
		 *  - m[i][j] vaut 0
		 * Resultat : un tableau r de longueur n^2+1 tel que, dans la tranche r[1..n^2]
		 * r[v] indique si v peut etre place en <i, j>
		 * 
		 */
		boolean[] possibleEn = new boolean[n*n+1]; //je respecte ici l'�nonc� mais pourquoi mettre la case de 0 ??
		possibleEn[0]=false;
		
		for(int k=1; k!=n*n+1; k++) 
		{
			if( ! ( presentLigne(m, k, i) || presentColonne(m,k,j) || presentRegion(m, k, i, j) ) )
				possibleEn[k]=true;
		}
		
		return possibleEn ; 
	} // lesPossiblesEn
	
	static String enClair (boolean[] t) {
		/*
		 * Prerequis : t.length != 0
		 * Resultat :
		 * une chaine contenant tous les indices i de la tranche [1..t.length-1] tels
		 * que t[i] soit vrai
		 */
		String r = "{" ;
		for (int i = 1; i < t.length; i++) {
			if (t[i]) {r = r + i + ", " ; }
		}
		if (r.length() != 1) {r = r.substring(0, r.length()-2);}
		return r + "}" ;
	} // enClair
	
	static int toutSeul (int [][] m, int i, int j) {
		/*
		 * Prerequis :
		 *  - m est un plateau de sudoku
		 *  - i et j sont compris entre 0 et n^2-1
		 *  - m[i][j] vaut 0
		 * Resultat :
		 *  - v 	si la seule valeur possible pour <i, j> est v
		 *  - -1 	dans les autres cas
		 * 
		 */
		boolean[] possibleEn = lesPossiblesEn(m, i, j);
		boolean test = false;
		int valeur_possible = -1;
		
		for(int k=1; k!= n*n+1; k++) 
		{
			if(possibleEn[k]) 
			{
				if(test) //plus d'une valeur est possible
				{
					return -1;
				}
				valeur_possible = k;
				test = true;
			}
		}
		
		return valeur_possible ; 
	} // toutSeul
	
	static void essais (String grille) {
		/*
		 * Prerequis : grille represente une grille de sudoku
		 * (les chiffres sont consideres comme ranges par lignes)
		 * 
		 * Effet :
		 * 1) affiche en clair la grille
		 * 2) affecte, tant que faire se peut, toutes les cases pour lesquelles il n'y
		 *    a qu'une seule possibilite
		 * 3) affiche en clair le resultat final
		 */
		int[][] m = aPartirDe(grille);
		System.out.println("Probleme\n\n"+enClair(m));

		int compteur = 100;
		while( compteur!=0 && !complet(m) )
		{
			for(int ligne=0; ligne!= n*n; ligne++) 
			{
				for(int colonne=0; colonne!= n*n; colonne++) 
				{
					if(m[ligne][colonne]!=0) 
					{
						continue;
					}
					int temp = toutSeul(m, ligne, colonne);
					if(temp!=-1) 
					{
						m[ligne][colonne] = temp;
						System.out.println("ligne: " + ligne);
						System.out.println("colonne : " + colonne);
						System.out.println("val : " + temp);
					}
				}
			}
			compteur-=1;
		}

		
		
		System.out.println("Il se peut qu'on ait avance\n\n"+enClair(m));
	} // essais


	static boolean complet (int[][] m)
	{
		/*
		 * Prerequis : grille represente une grille de sudoku
		 * (les chiffres sont consideres comme ranges par lignes)
		 * 
		 * Effet :
		 * 1) retourne vrai si la grille est complétée (toutes les cases différentes de 0), faux sinon
		 */
		for(int ligne=0; ligne!= n*n; ligne++) 
		{
			for(int colonne=0; colonne!= n*n; colonne++) 
			{
				if(m[ligne][colonne]==0)
					return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		String grille1 = 
			"040 001 006 \n" +
			"007 900 800 \n" +
			"190 086 074 \n" +
			"            \n" +
			"200 690 010 \n" +
			"030 405 090 \n" +
			"060 017 003 \n" +
			"            \n" +
			"910 750 042 \n" +
			"008 002 700 \n" +
			"400 300 080   " ;
		String grille2 = 
			"030 000 006 \n" +
			"000 702 300 \n" +
			"104 038 000 \n" +
			"            \n" +
			"300 020 810 \n" +
			"918 000 265 \n" +
			"062 050 007 \n" +
			"            \n" +
			"000 140 708 \n" +
			"001 209 000 \n" +
			"800 000 020   " ;
		
		/*
		String grille3 =
			"04 00 \n" +
			"00 40 \n" +
			"      \n" +
			"21 30 \n" +
			"40 00   "  ;
		*/


		essais(grille1);
		
		//essais(grille2);
		
		//essais(grille3); changer la valeur de n (n=2)
		
	}
		
}

/*
 * Exercice 7 :
 * La seule chose � modifier serait la valeur de n, (� condition que l'on ai des grilles � la taille convenue)
 * la valeur de n repr�sente la "taille" (longueur ou largeur) d'une r�gion
 * */
